<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    use HasFactory;

    function photo(){
        return $this->hasMany(Photo::class);
    }

    function albumHashtag(){
        return $this->hasMany(AlbumHastag::class);
    }

    function category(){
        return $this->belongsTo(Category::class,'category_id');
    }
}
