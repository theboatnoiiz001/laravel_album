<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hashtag extends Model
{
    use HasFactory;

    function albumHashtag(){
        return $this->hasMany(AlbumHastag::class);
    }
}
