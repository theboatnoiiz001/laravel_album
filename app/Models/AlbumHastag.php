<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AlbumHastag extends Model
{
    use HasFactory;

    public function hashtag(){
        return $this->belongsTo(Hashtag::class,'hashtag_id');
    }

    public function album(){
        return $this->belongsTo(Album::class,'album_id');
    }
}
