<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\AlbumHastag;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $albums = Album::latest()->get();
        return view('home',compact('albums'));
    }

    public function feedByHashtag($id){
        $albumHashtags = AlbumHastag::where('hashtag_id',$id)->latest()->get();
        
        return view('feedHashTag',compact('albumHashtags'));
    }

    public function feedByCategory($id){
        $albums = Album::where('category_id',$id)->latest()->get();
        
        return view('feedCategory',compact('albums'));
    }
}
