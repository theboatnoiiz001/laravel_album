<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\AlbumHastag;
use App\Models\Category;
use App\Models\Hashtag;
use App\Models\Photo;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $albums = Album::where('user_id',auth()->user()->id)->latest()->get();
        return view('albumAll',compact('albums'));
    }

    public function createAlbum(){
        $categorys = Category::latest()->get();
        return view('createAlbum',compact('categorys'));
    }

    public function create(Request $request){
        $request->validate([
            'title' => 'required|string|max:255',
            'desc' => 'required|string',
            'category' => 'required|numeric|exists:categories,id',
            'hashtag'=> 'required|nullable'
        ]);

        $album = new Album();
        $album->title = $request->title;
        $album->desc = $request->desc;
        $album->category_id = $request->category;
        $album->user_id = auth()->user()->id;
        $album->save();

        if($request->hashtag != ""){
            $hashtags = explode("#",$request->hashtag);
            if(count($hashtags) != 0){
                foreach($hashtags as $hashtag){
                    if(!empty(trim($hashtag))){
                        $new_hashtag = "#".trim($hashtag);
                        $hashtag = Hashtag::where('name',$new_hashtag)->get();
                        if($hashtag->count() == 0){
                            $hashtag = new Hashtag();
                            $hashtag->name = $new_hashtag;
                            $hashtag->save();
                        }else{
                            $hashtag = $hashtag[0];
                        }

                        $album_hashtag = new AlbumHastag();
                        $album_hashtag->album_id = $album->id;
                        $album_hashtag->hashtag_id = $hashtag->id;
                        $album_hashtag->save();
                    }
                }
            }
        }

        return redirect()->route('editAlbum',['id' => $album->id])->with('success','Create Album success.');

    }

    public function editAlbum($id){
        $album = Album::where([['user_id',auth()->user()->id],['id',$id]])->get();
        if($album->count()){
            $album = $album[0];
            $categorys = Category::all();
            return view('editAlbum',compact('album','categorys'));
        }else{
            return redirect()->route('home');
        }
        
    }

    public function editAlbumData(Request $request,$id){
        $request->validate([
            'title' => 'required|string|max:255',
            'desc' => 'required|string',
            'category' => 'required|numeric|exists:categories,id',
            'hashtag'=> 'required|nullable'
        ]);
        $album = Album::where([['user_id',auth()->user()->id],['id',$id]])->first();
        if($album){
            $album->title = $request->title;
            $album->desc = $request->desc;
            $album->category_id = $request->category;
            $album->save();

            foreach($album->albumHashtag as $old_hashtag){
                $old_hashtag->delete();
            }

            if($request->hashtag != ""){
                $hashtags = explode("#",$request->hashtag);
                if(count($hashtags) != 0){
                    foreach($hashtags as $hashtag){
                        if(!empty(trim($hashtag))){
                            $new_hashtag = "#".trim($hashtag);
                            $hashtag = Hashtag::where('name',$new_hashtag)->get();
                            if($hashtag->count() == 0){
                                $hashtag = new Hashtag();
                                $hashtag->name = $new_hashtag;
                                $hashtag->save();
                            }else{
                                $hashtag = $hashtag[0];
                            }
    
                            $album_hashtag = new AlbumHastag();
                            $album_hashtag->album_id = $album->id;
                            $album_hashtag->hashtag_id = $hashtag->id;
                            $album_hashtag->save();
                        }
                    }
                }
            }

            if($request->has('images')){
                foreach($request->file('images') as $image){
                    $genName = hexdec(uniqid());
                    $img_ext = strtolower($image->getClientOriginalExtension());
                    $img_name = $genName.'.'.$img_ext;
                    $upload_location = "images/photo_album/";
                    $full_path = $upload_location.$img_name;

                    $photo = new Photo();
                    $photo->photo_name = $full_path;
                    $photo->album_id = $album->id;
                    $photo->save();

                    $image->move($upload_location,$img_name);
                }
            }

            return redirect()->back()->with('success','Update Category success.');
        }else{
            return redirect()->route('home');
        }
    }

    public function deleteAlbum($id){
        $album = Album::where([['user_id',auth()->user()->id],['id',$id]])->first();
        if($album){

            foreach($album->photo as $photo){
                unlink($photo->photo_name);
                $photo->delete();
            }

            foreach($album->albumHashtag as $hashtag){
                $hashtag->delete();
            }

            $album->delete();
            return redirect()->back()->with('success','Update Album success.');
        }else{
            return redirect()->route('home');
        }
    }

    public function showAlbum($id){
        $album = Album::find($id);
        if($album){
            return view('albumShow',compact('album'));
        }else{
            return redirect()->route('home');
        }
    }

}
