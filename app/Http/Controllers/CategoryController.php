<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $categorys = Category::latest()->get();
        return view('category',compact('categorys'));
    }

    public function addCategory(Request $request){
        
        $request->validate([
            'name' => 'required|string|max:255|unique:categories,name'
        ]);

        $category = new Category();
        $category->name = $request->name;
        $category->save();


        return redirect()->back()->with('success','Create Category success.');
    }

    public function deleteCategory($id){
        $category = Category::find($id);
        
        if($category){
            $category->delete();
            return redirect()->back()->with('success','Delete Category success.');
        }else{
            return redirect()->back()->with('error','Not found this category.');
        }
    }

    public function updateCategory(Request $request){

        $request->validate([
            'idCategory' => 'required|integer|exists:categories,id',
            'nameCategory' => 'required|string|max:255|unique:categories,name'
        ]);

        $category = Category::find($request->idCategory);
        if($category){
            $category->name = $request->nameCategory;
            $category->save();
            return redirect()->back()->with('success','Update Category success.');
        }else{
            return redirect()->back()->with('error','Not found this category.');
        }
    }
}
