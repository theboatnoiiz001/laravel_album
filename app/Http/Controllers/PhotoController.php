<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\AlbumHastag;
use App\Models\Photo;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    //
    public function delete($id){
        $photo = Photo::find($id);
        if($photo){
            if(empty($photo->album->user_id)){
                return redirect()->back()->with('error','Something Wrong!.');
            }
            if($photo->album->user_id == auth()->user()->id){
                $photo->delete();
                return redirect()->back()->with('success','Delete Photo successfuly.');
            }else{
                return redirect()->back()->with('error','Not have Premission to access.');
            }
        }else{
            return redirect()->back()->with('error','Not found this photo.');
        }
    }

    
}
