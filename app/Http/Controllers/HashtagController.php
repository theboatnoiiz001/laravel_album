<?php

namespace App\Http\Controllers;

use App\Models\Hashtag;
use Illuminate\Http\Request;

class HashtagController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $hashtags = Hashtag::latest()->get();
        return view('hashtag',compact('hashtags'));
    }

    public function addHashtag(Request $request){
        
        $request->validate([
            'name' => 'required|string|max:255|unique:categories,name'
        ]);

        $hashtag = new Hashtag();
        $hashtag->name = $request->name;
        $hashtag->save();


        return redirect()->back()->with('success','Create Hashtag success.');
    }

    public function deleteHashtag($id){
        $hashtag = Hashtag::find($id);
        
        if($hashtag){
            foreach($hashtag->albumHashtag as $hashtagLog){
                $hashtagLog->delete();
            }
            $hashtag->delete();
            return redirect()->back()->with('success','Delete Hashtag success.');
        }else{
            return redirect()->back()->with('error','Not found this hashtag.');
        }
    }

    public function updateHashtag(Request $request){

        $request->validate([
            'idHashtag' => 'required|integer|exists:hashtags,id',
            'nameHashtag' => 'required|string|max:255|unique:hashtags,name'
        ]);

        $hashtag = Hashtag::find($request->idHashtag);
        if($hashtag){
            $hashtag->name = $request->nameHashtag;
            $hashtag->save();
            return redirect()->back()->with('success','Update Hashtag success.');
        }else{
            return redirect()->back()->with('error','Not found this hashtag.');
        }
    }

}
