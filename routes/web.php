<?php

use App\Http\Controllers\AlbumController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HashtagController;
use App\Http\Controllers\PhotoController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

//Feed
Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/home/hashtag/{id}', [HomeController::class, 'feedByHashtag'])->name('feedByHashtag');
Route::get('/home/category/{id}', [HomeController::class, 'feedByCategory'])->name('feedByCategory');

//Category
Route::get('/category', [CategoryController::class, 'index'])->name('category');
Route::post('/category/add', [CategoryController::class, 'addCategory'])->name('addcategory');
Route::post('/category/delete/{id}', [CategoryController::class, 'deleteCategory'])->name('deletecategory');
Route::post("/category/update",[CategoryController::class,'updateCategory']);

//hashtag
Route::get('/hashtag', [HashtagController::class, 'index'])->name('hashtag');
Route::post('/hashtag/add', [HashtagController::class, 'addHashtag'])->name('addHashtag');
Route::post('/hashtag/delete/{id}', [HashtagController::class, 'deleteHashtag'])->name('deleteHashtag');
Route::post("/hashtag/update",[HashtagController::class,'updateHashtag']);

//Album
Route::get('/album/my', [AlbumController::class, 'index'])->name('myAlbum');
Route::get('/albumCreate', [AlbumController::class, 'createAlbum'])->name('createAlbum');
Route::post('/albumCreate', [AlbumController::class, 'create'])->name('createAlbumPost');
Route::get('/album/{id}', [AlbumController::class, 'showAlbum'])->name('showAlbum');
Route::get('/album/edit/{id}', [AlbumController::class, 'editAlbum'])->name('editAlbum');
Route::post('/album/edit/{id}', [AlbumController::class, 'editAlbumData'])->name('editAlbumData');
Route::post('/album/delete/{id}', [AlbumController::class, 'deleteAlbum'])->name('deleteAlbum');

//Photo
Route::post('/photo/delete/{id}',[PhotoController::class,'delete'])->name('deletePhoto');