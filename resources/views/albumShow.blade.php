@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 row">
                @if (session('success'))
                    <div class="alert alert-success"><b>{{ session('success') }}</b></div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger"><b>{{ session('error') }}</b></div>
                @endif
                <div class="col-md-4 mb-3">
                    <div class="card">
                        <div class="card-header"><i class="fal fa-image-polaroid"></i> Album Data</div>
                        <div class="card-body">

                            <form action="{{ route('editAlbumData', ['id' => $album->id]) }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <label>Title:</label>
                                <b>{{ $album->title }}</b>
                                <br>
                                <label>Description:</label>
                                <b>{{ $album->desc }}</b>
                                <br>
                                <label>Hashtag:</label>
                                @foreach ($album->albumHashtag as $hashtag)
                                    <a href="{{route('feedByHashtag',['id' => $hashtag->hashtag->id])}}">{{ $hashtag->hashtag->name }}</a>
                                @endforeach
                                <p>
                                    Category: <a
                                        href="{{route('feedByCategory',['id' => $album->category->id])}}">{{ $album->category->name }}</a>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"><i class="fas fa-photo-video"></i> Photo in Album</div>
                        <div class="card-body">
                            @if ($album->photo->count() == 0)
                                <div class="text-center p-5">
                                    <span>ยังไม่มีรูปในอัลบัมนี้</span>
                                </div>
                            @endif
                            <div class="row">


                                @foreach ($album->photo as $photo)
                                    <div class="col-lg-4 d-none d-lg-block mb-3">
                                        <div class="card">
                                            <img src="{{ asset($photo->photo_name) }}" data-action="zoom"
                                                class="card-img-top" alt="Sunset Over the Sea" />

                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
