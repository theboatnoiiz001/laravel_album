@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 row">
                @if (session('success'))
                    <div class="alert alert-success"><b>{{ session('success') }}</b></div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger"><b>{{ session('error') }}</b></div>
                @endif
                <div class="col-md-4 mb-3">
                    <div class="card">
                        <div class="card-header"><i class="fas fa-edit"></i> Edit Album</div>
                        <div class="card-body">

                            <form action="{{ route('editAlbumData', ['id' => $album->id]) }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <label>Title:</label>
                                <input type="text" class="form-control" name="title" value="{{ $album->title }}">
                                <label>Description:</label>
                                <textarea class="form-control" name="desc">{{ $album->desc }}</textarea>
                                <label>Hashtag:</label>
                                <input type="text" class="form-control" name="hashtag"
                                    value="@foreach ($album->albumHashtag as $hashtag) {{ $hashtag->hashtag->name }} @endforeach">
                                <label for="category">Category</label>
                                <select class="form-select" name="category" required>
                                    <option value="{{ $album->category_id }}" selected>{{ $album->category->name }}
                                        (Selected)</option>
                                    @foreach ($categorys as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                                <br>
                                <label for="file">Upload Photo</label>
                                <input type="file" name="images[]" class="form-control" accept="image/*" multiple>

                                <div class="text-center mt-4">
                                    <input type="submit" class="btn btn-primary" value="save">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header"><i class="fas fa-photo-video"></i> Photo in Album</div>
                        <div class="card-body">
                            @if ($album->photo->count() == 0)
                                <div class="text-center p-5">
                                    <span>คุณยังไม่มีรูปในอัลบัมนี้</span>
                                </div>
                            @endif
                            <div class="row">


                                @foreach ($album->photo as $photo)
                                    <div class="col-lg-4 d-none d-lg-block mb-3">
                                        <div class="card">
                                            <img src="{{ asset($photo->photo_name) }}" data-action="zoom" class="card-img-top"
                                                alt="Sunset Over the Sea" />
                                            <div class="card-body text-center">
                                                <form action="{{route('deletePhoto',['id' => $photo->id])}}" method="POST">
                                                    @csrf
                                                    <input type="submit" class="btn btn-danger" value="Delete">
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection
