@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="text-center mb-2">
                    <a href="{{ route('createAlbum') }}" class="btn btn-primary m-2"><i class="fas fa-images"></i>
                        สร้างอัลบัม</a>
                    <a href="{{ route('myAlbum') }}" class="btn btn-warning m-2"><i class="fas fa-camera"></i>
                        อัลบัมของฉัน</a>
                </div>
                @if ($albums->count() == 0)
                    <div class="card mt-5">
                        <div class="card-body">
                            <div class="text-center m-2">
                                <h4 class="text-danger"> ยังไม่มีการโพสต์อัลบัมใน Category นี้ </h4>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="text-left m-3">
                        <h1>{{ $albums->first()->category->name }}</h1>
                    </div>
                @endif
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                @foreach ($albums as $album)
                    
                    @php
                        $img = '';
                        if ($album->photo->count() == 0) {
                            $img = 'https://mdbcdn.b-cdn.net/img/new/standard/nature/182.webp';
                        } else {
                            $img = asset($album->photo->first()->photo_name);
                        }
                    @endphp

                    <div class="card m-4">
                        <img class="card-img-top" src="{{ $img }}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{ $album->title }}</h5>
                            <p class="card-text">
                                {{ $album->desc }}
                            </p>
                            <hr>
                            <p>
                                Category: <a href="{{route('feedByCategory',['id' => $album->category->id])}}">{{ $album->category->name }}</a>
                            </p>
                            @if ($album->albumHashtag->count() != 0)
                                <p>Hashtags:
                                    @foreach ($album->albumHashtag as $hashtag)
                                        <a
                                            href="{{ route('feedByHashtag', ['id' => $hashtag->hashtag->id]) }}">{{ $hashtag->hashtag->name }}</a>
                                    @endforeach
                                    <hr>
                            @endif
                            <div class="text-center">
                                <a href="{{ route('showAlbum', ['id' => $album->id]) }}" class="btn btn-primary"><i
                                        class="fas fa-images"></i> Open</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
