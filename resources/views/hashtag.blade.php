@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 row">
                @if (session('success'))
                    <div class="alert alert-success"><b>{{ session('success') }}</b></div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger"><b>{{ session('error') }}</b></div>
                @endif
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">Add Hashtag</div>
                        <div class="card-body">
                            <form action="{{ url('/hashtag/add') }}" method="post">
                                @csrf
                                <label for="name">Name:</label>
                                <input type="text" class="form-control" name="name" placeholder="Name Hashtag" required>
                                @error('name')
                                    <span class="text-danger mt-2">{{ $message }}</span>
                                @enderror
                                <div class="text-center mt-2">
                                    <input type="submit" class="btn btn-primary" value="Add">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-header"><i class="fab fa-slack-hash"></i> List Hashtag</div>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#ID</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Edit</th>
                                        <th scope="col">Delete</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach ($hashtags as $hashtag)
                                        <tr>
                                            <th scope="row">{{ $hashtag->id }}</th>
                                            <td id="name{{ $hashtag->id }}">{{ $hashtag->name }}</td>
                                            <td><button class="btn btn-warning" data-bs-toggle="modal"
                                                    data-bs-target="#editHashtag"
                                                    onclick="editHashtag({{ $hashtag->id }})"><i
                                                        class="fas fa-edit"></i> Edit</button>
                                            </td>
                                            <td>
                                                <form action="{{ url('/hashtag/delete/' . $hashtag->id) }}"
                                                    method="post">
                                                    @csrf <input type="submit" class="btn btn-danger" value="Delete"></form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="modal fade" id="editHashtag" data-bs-backdrop="static" data-bs-keyboard="false"
                                tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="staticBackdropLabel">Edit Hashtag</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <label>Name</label>
                                            <form action="{{ url('hashtag/update') }}" method="POST">
                                                @csrf
                                                <input type="text" class="form-control" id="nameHashtag"
                                                    name="nameHashtag" value="">
                                                <input type="hidden" class="form-control" id="idHashtag"
                                                    name="idHashtag" value="">
                                                <div class="text-center mt-3">
                                                    <input type="submit" class="btn btn-primary" value="Save chagne">
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function editHashtag(id) {
            $("#nameHashtag").val($("#name" + id).text())
            $("#idHashtag").val(id)
        }
    </script>
@endsection
