@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 row">
                @if (session('success'))
                    <div class="alert alert-success"><b>{{ session('success') }}</b></div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger"><b>{{ session('error') }}</b></div>
                @endif
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header"><i class="fas fa-photo-video"></i> My Album</div>
                        <div class="card-body">


                            @if ($albums->count() == 0)
                                <div class="text-center m-5"><span>คุณยังไม่เคยสร้างอัลบัม <a
                                            href="{{ route('createAlbum') }}">สร้างอัลบัม</a></span></div>
                            @endif
                            <div class="row">
                                @foreach ($albums as $album)
                                    @php
                                        $img = '';
                                        if ($album->photo->count() == 0) {
                                            $img = 'https://mdbcdn.b-cdn.net/img/new/standard/nature/182.webp';
                                        } else {
                                            $img = asset($album->photo->first()->photo_name);
                                        }
                                    @endphp

                                    <div class="col-lg-3 d-none d-lg-block mb-3">
                                        <div class="card">
                                            <img src="{{ $img }}" class="card-img-top"
                                                alt="Sunset Over the Sea" />
                                            <div class="card-body">
                                                <h5 class="card-title">{{ $album->title }}</h5>
                                                <p class="card-text">
                                                    {{ $album->desc }}
                                                </p>
                                                <hr>
                                                <p>
                                                    Category: <a
                                                        href="{{route('feedByCategory',['id' => $album->category->id])}}">{{ $album->category->name }}</a>
                                                </p>
                                                @if ($album->albumHashtag->count() != 0)
                                                    <p>Hashtags:
                                                        @foreach ($album->albumHashtag as $hashtag)
                                                            <a
                                                            href="{{route('feedByHashtag',['id' => $hashtag->hashtag->id])}}">{{ $hashtag->hashtag->name }}</a>
                                                        @endforeach
                                                        <hr>
                                                @endif
                                                <div class="text-center">
                                                    <form action="{{ route('deleteAlbum', ['id' => $album->id]) }}"
                                                        method="POST">
                                                        @csrf
                                                        <a href="{{ route('showAlbum', ['id' => $album->id]) }}"
                                                            class="btn btn-primary"><i class="fas fa-images"></i> Open</a>
                                                        <a href="{{ route('editAlbum', ['id' => $album->id]) }}"
                                                            class="btn btn-warning"><i class="fas fa-edit"></i> Edit</a>

                                                        <input type="submit" class="btn btn-danger"
                                                            onclick="return confirm('Are you sure you want to delete this item?');"
                                                            value="Delete">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
